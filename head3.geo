R = 50;
cs = 5;
cd = 2;
l = 0.15*R;
sq2 = 0.70710678118654752440;
sq3 = 0.57735026918962576450;
Point(21)  = {  0,  0+cs/2,  0, l};
Point(22)  = { +R,  0+cs/2,  0, l};
Point(23)  = { -R,  0+cs/2,  0, l};
Point(24)  = {  0, +R+cs/2,  0, l};
Point(26)  = {  0,  0+cs/2, +R, l};

Circle(262) = {26,21, 22};
Circle(263) = {26,21, 23};
Circle(264) = {26,21, 24};

Circle(224) = {22,21, 24};
Circle(243) = {24,21, 23};

Line(223) = {22,23};

Line Loop(21)  = {262, 224,-264};
Line Loop(22)  = {264, 243,-263};

Line Loop(25)  = {224, 243,-223};
Line Loop(26)  = {262, 223,-263};

Surface(121) = {21};
Surface(122) = {22};

Surface(125) = {25};
Surface(126) = {26};

Surface Loop(202) = {121,122,125,126};
Volume(302) = {202};

Point(11)  = {  0,  0-cs/2,  0, l};
Point(12)  = { +R,  0-cs/2,  0, l};
Point(13)  = { -R,  0-cs/2,  0, l};
Point(14)  = {  0, -R-cs/2,  0, l};
Point(16)  = {  0,  0-cs/2, +R, l};

Circle(162) = {16,11, 12};
Circle(163) = {16,11, 13};
Circle(164) = {16,11, 14};

Circle(124) = {12,11, 14};
Circle(143) = {14,11, 13};

Line(123) = {12,13};

Line Loop(11)  = {162, 124,-164};
Line Loop(12)  = {164, 143,-163};

Line Loop(15)  = {124, 143,-123};
Line Loop(16)  = {162, 123,-163};

Surface(111) = {11};
Surface(112) = {12};

Surface(115) = {15};
Surface(116) = {16};

Surface Loop(201) = {111,112,115,116};
Volume(301) = {201};

Point(1)  = {  0,  0,  0, l};
Point(2)  = { +R+3/2*cs,  0,  0, l};
Point(3)  = { -R-3/2*cs,  0,  0, l};
Point(4)  = {  0, +R+3/2*cs,  0, l};
Point(5)  = {  0, -R-3/2*cs,  0, l};
Point(6)  = {  0,  0, +R+3/2*cs, l};

Circle(62) = {6,1, 2};
Circle(63) = {6,1, 3};
Circle(64) = {6,1, 4};
Circle(65) = {6,1, 5};

Circle(24) = {2,1, 4};
Circle(43) = {4,1, 3};
Circle(35) = {3,1, 5};
Circle(52) = {5,1, 2};

Line Loop(1)  = {62, 24,-64};
Line Loop(2)  = {64, 43,-63};
Line Loop(3)  = {63, 35,-65};
Line Loop(4)  = {65, 52,-62};

Line Loop(5)  = {24,43,35,52};

Surface(101) = {1};
Surface(102) = {2};
Surface(103) = {3};
Surface(104) = {4};

Surface(105) = {5};

Surface Loop(200) = {101,102,103,104,105};
Volume(300) = {200};

Point(31)  = {  0,  0,  0, l};
Point(32)  = { +R+3/2*cs+cd,  0,  0, l};
Point(33)  = { -R-3/2*cs-cd,  0,  0, l};
Point(34)  = {  0, +R+3/2*cs+cd,  0, l};
Point(35)  = {  0, -R-3/2*cs-cd,  0, l};
Point(36)  = {  0,  0, +R+3/2*cs+cd, l};

Circle(362) = {36,31,32};
Circle(363) = {36,31,33};
Circle(364) = {36,31,34};
Circle(365) = {36,31,35};

Circle(324) = {32,31,34};
Circle(343) = {34,31,33};
Circle(335) = {33,31,35};
Circle(352) = {35,31,32};

Line Loop(31)  = {362,324,-364};
Line Loop(32)  = {364,343,-363};
Line Loop(33)  = {363,335,-365};
Line Loop(34)  = {365,352,-362};

Line Loop(35)  = {324,343,335,352};

Surface(131) = {31};
Surface(132) = {32};
Surface(133) = {33};
Surface(134) = {34};

Surface(135) = {35};

Surface Loop(203) = {131,132,133,134,135};
Volume(303) = {203};