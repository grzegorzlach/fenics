R = 50;
cs = 5;
l = 0.15*R;
sq2 = 0.70710678118654752440;
sq3 = 0.57735026918962576450;
Point(21)  = {  0,  0+cs/2,  0, l};
Point(22)  = { +R,  0+cs/2,  0, l};
Point(23)  = { -R,  0+cs/2,  0, l};
Point(24)  = {  0, +R+cs/2,  0, l};
Point(26)  = {  0,  0+cs/2, +R, l};

Circle(262) = {26,21, 22};
Circle(263) = {26,21, 23};
Circle(264) = {26,21, 24};

Circle(224) = {22,21, 24};
Circle(243) = {24,21, 23};

Line(223) = {22,23};

Line Loop(21)  = {262, 224,-264};
Line Loop(22)  = {264, 243,-263};

Line Loop(25)  = {224, 243,-223};
Line Loop(26)  = {262, 223,-263};

Surface(121) = {21};
Surface(122) = {22};

Surface(125) = {25};
Surface(126) = {26};

Surface Loop(202) = {121,122,125,126};
Volume(302) = {202};

Point(11)  = {  0,  0-cs/2,  0, l};
Point(12)  = { +R,  0-cs/2,  0, l};
Point(13)  = { -R,  0-cs/2,  0, l};
Point(14)  = {  0, -R-cs/2,  0, l};
Point(16)  = {  0,  0-cs/2, +R, l};

Circle(162) = {16,11, 12};
Circle(163) = {16,11, 13};
Circle(164) = {16,11, 14};

Circle(124) = {12,11, 14};
Circle(143) = {14,11, 13};

Line(123) = {12,13};

Line Loop(11)  = {162, 124,-164};
Line Loop(12)  = {164, 143,-163};

Line Loop(15)  = {124, 143,-123};
Line Loop(16)  = {162, 123,-163};

Surface(111) = {11};
Surface(112) = {12};

Surface(115) = {15};
Surface(116) = {16};

Surface Loop(201) = {111,112,115,116};
Volume(301) = {201};

