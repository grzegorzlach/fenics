R = 1;
l = 0.15*R;
sq2 = 0.70710678118654752440;
sq3 = 0.57735026918962576450;
Point(1)  = {  0,  0,  0, l};
Point(2)  = { +R,  0,  0, l};
Point(3)  = { -R,  0,  0, l};
Point(4)  = {  0, +R,  0, l};
Point(5)  = {  0, -R,  0, l};
Point(6)  = {  0,  0, +R, l};

Circle(62) = {6,1, 2};
Circle(63) = {6,1, 3};
Circle(64) = {6,1, 4};
Circle(65) = {6,1, 5};

Circle(24) = {2,1, 4};
Circle(43) = {4,1, 3};
Circle(35) = {3,1, 5};
Circle(52) = {5,1, 2};

//Line(12) = (1,2);
//Line(13) = (1,3);
//Line(14) = (1,4);
//Line(15) = (1,5);

Line Loop(1)  = {62, 24,-64};
Line Loop(2)  = {64, 43,-63};
Line Loop(3)  = {63, 35,-65};
Line Loop(4)  = {65, 52,-62};

Line Loop(5)  = {24,43,35,52};

Ruled Surface(101) = {1};
Ruled Surface(102) = {2};
Ruled Surface(103) = {3};
Ruled Surface(104) = {4};

Plane Surface(105) = {5};

Surface Loop(200) = {101,102,103,104,105};
Volume(300) = {200};

Physical Surface(1) = {200};
Physical Volume(2) = 300;
