"""This demo program solves the mixed formulation of Laplace equation:

    sigma - grad(u) = 0
       - div(sigma) = 0 (been f, GeL)

The corresponding weak (variational problem)

    <sigma, tau> + <div(tau), u>   = 0                          for all tau
                 - <div(sigma), v> = 0    (been <f, v>, GeL)    for all v

is solved using BDM (Brezzi-Douglas-Marini) elements of degree k for
(sigma, tau) and DG (discontinuous Galerkin) elements of degree k - 1
for (u, v).

Original implementation: ../cpp/main.cpp by Anders Logg and Marie Rognes
"""

""" GeL
Input tested with:
https://homerreid.github.io/buff-em-documentation/

Acces to mesh subdomains:
https://fenicsproject.org/olddocs/dolfin/1.6.0/python/demo/documented/subdomains/python/documentation.html

Not used but worth checking:
https://uma.ensta-paristech.fr/soft/XLiFE++/
"""

from dolfin import *
from pymesh import *
from numpy import *

# Read mesh

#mesh = pymesh.load_mesh("head3.msh")
mesh = Mesh("head3.xml")

# Define function spaces and mixed (product) space
BDM = FunctionSpace(mesh, "BDM", 1)
DG = FunctionSpace(mesh, "DG", 0)
W = BDM * DG

# Define trial and test functions
(sigma, u) = TrialFunctions(W)
(tau, v) = TestFunctions(W)

# Define variational form
a = (dot(sigma, tau) + div(tau)*u + div(sigma)*v)*dx
L = - 0*v*dx

# Define function G such that G \cdot n = g
class BoundarySource(Expression):
    def __init__(self, mesh):
        self.mesh = mesh
    def eval_cell(self, values, x, ufc_cell):
        cell = Cell(self.mesh, ufc_cell.index)
        n = cell.normal(ufc_cell.local_facet)
        g = x[0]      # potential on boundary
        values[0] = g*n[0]
        values[1] = g*n[1]
    def value_shape(self):
        return (2,)

G = BoundarySource(mesh)

# Define essential boundary
def boundary(x):
    return x[1] < DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS

bc = DirichletBC(W.sub(0), G, boundary)

# Compute solution
w = Function(W)
solve(a == L, w, bc)
(sigma, u) = w.split()

# Plot sigma and u
plot(sigma)
plot(u)
interactive()
